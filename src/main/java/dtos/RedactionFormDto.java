package dtos;

import entities.Article;

public class RedactionFormDto {
    private Article.THEMES theme;
    private String titre;
    private String contenu;

    public Article.THEMES getTheme() {
        return theme;
    }

    public void setTheme(Article.THEMES theme) {
        this.theme = theme;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String contenu) {
        this.contenu = contenu;
    }
}
