package entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
public class Utilisateur {
    public enum ROLES {JOURNALISTE, ASSISTANT, CHEF};

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idU;

    @Column(unique=true)
    private String login;
    private String password;
    private ROLES role;

    @OneToMany(mappedBy = "auteur")
    private List<Article> articles;

    public Utilisateur(){
        articles = new ArrayList<>();
    }

    public Utilisateur(String login, String password, ROLES role) {
        this.login = login;
        this.password = password;
        this.role = role;
        this.articles = new ArrayList<>();
    }

    public int getIdU() {
        return idU;
    }

    public void setIdU(int idU) {
        this.idU = idU;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public ROLES getRole() {
        return role;
    }

    public void setRole(ROLES role) {
        this.role = role;
    }

    public List<Article> getArticles() {
        return articles;
    }

    public void addArticle(Article a) {
        articles.add(a);
    }

}
