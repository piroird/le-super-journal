package entities;

import javax.persistence.*;
import java.util.*;

@Entity
public class Journal {
    public enum STATUS {ATTENTE, PUBLIE}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idJ;

    @Column(unique = true)
    private int numero;
    private STATUS status;
    private Date datePublication;

    @OneToMany(mappedBy = "journal")
    private List<Article> articles;

    public Journal(){
        this.articles = new ArrayList<>();
    }

    public Journal(int numero){
        this.numero = numero;
        this.status = STATUS.ATTENTE;
        this.articles = new ArrayList<>();
    }

    public int getIdJ() {
        return idJ;
    }

    public void setIdJ(int idJ) {
        this.idJ = idJ;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        this.status = status;
    }

    public Date getDatePublication() {
        return datePublication;
    }

    public void setDatePublication(Date datePublication) {
        this.datePublication = datePublication;
    }

    public List<Article> getArticles() {
        Collections.sort(this.articles, Comparator.comparing(Article::getPosition));
        return articles;
    }

    public void setArticles(List<Article> articles) {
        this.articles = articles;
    }

    public void addArticle(Article a){
        this.articles.add(a);
    }
}
