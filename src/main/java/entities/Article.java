package entities;

import javax.persistence.*;
import java.sql.Date;

@Entity
public class Article {
    public enum STATUS {REJETE, ATTENTE, VALIDER}
    public enum THEMES {POLITIQUE, ECONOMIQUE, MONDE, SPORT, OPINION, CULTURE}

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int idA;

    private String titre;
    @Lob
    private String contenu;
    private Date dateCreation;
    private STATUS status;
    private THEMES theme;

    @ManyToOne
    private Utilisateur auteur;
    @ManyToOne
    private Journal journal;
    private Integer position;

    public Article(){}
    public Article(String titre, String contenu, STATUS status, THEMES theme, Utilisateur auteur) {
        this.titre = titre;
        this.contenu = contenu;
        this.status = status;
        this.theme = theme;
        this.auteur = auteur;
        // Date du jour par défaut
        this.dateCreation = new Date(System.currentTimeMillis());
    }

    public int getIdA() {
        return idA;
    }

    public void setIdA(int idA) {
        this.idA = idA;
    }

    public String getTitre() {
        return titre;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public String getContenu() {
        return contenu;
    }

    public void setContenu(String content) {
        this.contenu = content;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }

    public STATUS getStatus() {
        return status;
    }

    public void setStatus(STATUS status) {
        if(status == STATUS.ATTENTE)
            this.position = null;
        this.status = status;
    }

    public THEMES getTheme() {
        return theme;
    }

    public void setTheme(THEMES theme) {
        this.theme = theme;
    }

    public Utilisateur getAuteur() {
        return auteur;
    }

    public void setAuteur(Utilisateur auteur) {
        this.auteur = auteur;
    }

    public Journal getJournal() {
        return journal;
    }

    public void setJournal(Journal journal) {
        this.journal = journal;
    }

    public Integer getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }
}
