package controllers;

import dtos.LoginFormDto;
import dtos.RedactionFormDto;
import entities.Article;
import entities.Journal;
import entities.Utilisateur;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import services.Facade;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

@Controller
@SessionAttributes("user")
@RequestMapping("/")
public class MainController {
    @Autowired
    Facade facade;

    private void verifUserRight(Model model, Utilisateur.ROLES... roles){
        Utilisateur u = (Utilisateur) model.getAttribute("user");
        if(u != null){
            for(Utilisateur.ROLES r : roles){
                if(u.getRole() == r) return;
            }
        }
        throw new AccessUnauthorizedException();
    }

    @RequestMapping("")
    public String accueil(Model model) {
        List<Journal> l2 = facade.getJournaux(Journal.STATUS.PUBLIE, true);
        Collections.sort(l2, (j1, j2) -> j2.getDatePublication().compareTo(j1.getDatePublication()));
        model.addAttribute("journaux", l2);
        return "accueil";
    }

    @RequestMapping(value = "login", method = RequestMethod.GET)
    public String login_get(Model model) {
        model.addAttribute("mode", "con");
        model.addAttribute("loginFormDto", new LoginFormDto());
        return "login";
    }

    @RequestMapping(value = "login", method = RequestMethod.POST)
    public String login_post(@ModelAttribute LoginFormDto loginFormDto, Model model) {
        Utilisateur u = facade.connexion(loginFormDto);
        if (u != null) {
            model.addAttribute("user", u);
            return "redirect:/";
        }
        model.addAttribute("mode", "con");
        model.addAttribute("msg", "Identifiant ou mot de passe invalide.");
        return "login";
    }

    @RequestMapping(value = "logout")
    public String logout(SessionStatus status, Model model) {
        status.setComplete();
        model.addAttribute("mode", "con");
        model.addAttribute("user", null);
        return "redirect:login";
    }

    @RequestMapping(value = "inscription", method = RequestMethod.GET)
    public String register_get(Model model) {
        model.addAttribute("mode", "inscr");
        model.addAttribute("loginFormDto", new LoginFormDto());
        return "login";
    }

    @RequestMapping(value = "inscription", method = RequestMethod.POST)
    public String register_post(@ModelAttribute LoginFormDto loginFormDto, Model model) {
        try{
            Utilisateur u = facade.inscription(loginFormDto);
            if (u != null) {
                model.addAttribute("user", u);
                return "redirect:/";
            }
        }
        catch (Exception e){}

        model.addAttribute("mode", "inscr");
        model.addAttribute("msg", "L'utilisateur existe déjà.");
        return "login";
    }

    @RequestMapping("trierArticles")
    public String trierArticles(Model model){
        verifUserRight(model, Utilisateur.ROLES.ASSISTANT, Utilisateur.ROLES.CHEF);
        model.addAttribute("titre", "Tri des articles");

        List<Article> articles = facade.getArticles(Article.STATUS.ATTENTE);
        model.addAttribute("articles", articles);

        Utilisateur u = (Utilisateur) model.getAttribute("user");
        if(u.getRole() == Utilisateur.ROLES.CHEF){
            List<Journal> journaux = facade.getJournaux(Journal.STATUS.ATTENTE, true);
            Collections.sort(journaux, Comparator.comparing(Journal::getIdJ));
            model.addAttribute("journaux", journaux);
        }

        return "trierArticles";
    }

    @RequestMapping("voirArticle/{id}")
    public String voirArticle(@PathVariable int id, Model model){
        verifUserRight(model, Utilisateur.ROLES.ASSISTANT, Utilisateur.ROLES.CHEF);
        Article a = facade.getArticle(id);
        if(a == null || a.getStatus() != Article.STATUS.ATTENTE) throw new NotFoundException();
        model.addAttribute("article", a);

        List<Journal> journalList = facade.getJournaux(Journal.STATUS.ATTENTE, false);
        model.addAttribute("listeJournal", journalList);
        return "voirArticle";
    }

    @RequestMapping(value = "redaction", method = RequestMethod.GET)
    public String article_get(Model model) {
        model.addAttribute("redactionFormDto", new RedactionFormDto());
        return "redaction";
    }

    @RequestMapping(value = "redaction", method = RequestMethod.POST)
    public String article_post(@ModelAttribute RedactionFormDto redactionFormDto, Model model) {
        verifUserRight(model, Utilisateur.ROLES.JOURNALISTE);

        Utilisateur u = (Utilisateur) model.getAttribute("user");
        facade.redaction(redactionFormDto, u);
        return "redirect:/";
    }

    @RequestMapping("voirJournal/{id}")
    public String voirJournal(@PathVariable int id, Model model) {
        Journal j = facade.getJournal(id);
        if (j == null) throw new NotFoundException();
        model.addAttribute("journal", j);
        return "voirJournal";
    }

    @RequestMapping(value = "rejeterArticle/{id}", method = RequestMethod.POST)
    public String rejeterArticle(@PathVariable int id, Model model){
        verifUserRight(model, Utilisateur.ROLES.ASSISTANT, Utilisateur.ROLES.CHEF);
        if(!facade.rejeterArticle(id))
            throw new NotFoundException();
        return "redirect:/trierArticles";
    }

    @RequestMapping(value = "ajouterArticle", method = RequestMethod.POST)
    public String ajouterArticle(Model model, String idArticle, String idJournal){
        verifUserRight(model, Utilisateur.ROLES.ASSISTANT, Utilisateur.ROLES.CHEF);
        if(!facade.ajouterArticle(Integer.parseInt(idArticle), Integer.parseInt(idJournal))){
            throw new NotFoundException();
        }
        return "redirect:/trierArticles";
    }

    @RequestMapping(value = "newJournal", method = RequestMethod.POST)
    public String newJournal(Model model){
        verifUserRight(model, Utilisateur.ROLES.CHEF);
        facade.creerJournal();
        return "redirect:/trierArticles";
    }

    @RequestMapping("gererJournal/{id}")
    public String gererJournal(@PathVariable int id, Model model){
        verifUserRight(model, Utilisateur.ROLES.CHEF);
        model.addAttribute("titre", "Gérer le journal");
        model.addAttribute("idJ", id);

        Journal j = facade.getJournal(id);
        if(j == null) throw new NotFoundException();

        model.addAttribute("articles", j.getArticles());
        return "gererJournal";
    }

    @RequestMapping(value = "publierJournal/{id}", method = RequestMethod.POST)
    public String publierJournal(@PathVariable int id, Model model){
        verifUserRight(model, Utilisateur.ROLES.CHEF);
        if(!facade.publierJournal(id)){
            throw new NotFoundException();
        }
        return "redirect:/voirJournal/"+id;
    }

    @RequestMapping(value = "deplacerArticle/{idJ}", method = RequestMethod.POST)
    public String deplacerArticle(@PathVariable int idJ, Model model, String idArticle, String position){
        verifUserRight(model, Utilisateur.ROLES.CHEF);
        facade.deplacerArticle(idJ, Integer.parseInt(idArticle), Integer.parseInt(position));
        return "redirect:/gererJournal/"+idJ;
    }

    @RequestMapping(value = "rejeterArticleJournal/{idJ}/{idA}", method = RequestMethod.POST)
    public String rejeterArticleJournal(@PathVariable int idJ, @PathVariable int idA, Model model){
        verifUserRight(model, Utilisateur.ROLES.CHEF);
        facade.rejeterArticleJournal(idJ, idA);
        return "redirect:/gererJournal/"+idJ;
    }
}
