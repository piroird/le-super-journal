package services;

import dtos.LoginFormDto;
import dtos.RedactionFormDto;
import entities.Article;
import entities.Journal;
import entities.Utilisateur;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.sql.Date;
import java.util.List;

@Service
public class Facade {
    @PersistenceContext
    EntityManager em;
    
    @Transactional
    public Utilisateur connexion(LoginFormDto dto){
        try{
            Utilisateur u = em.createQuery("select u from Utilisateur u where u.login = :login", Utilisateur.class)
                    .setParameter("login", dto.getLogin())
                    .getSingleResult();

            if(u.getPassword().equals(dto.getPassword()))
                return u;
            return null;
        }
        catch (Exception e){
            return null;
        }
    }

    @Transactional
    public Utilisateur inscription(LoginFormDto dto){
        Utilisateur u = new Utilisateur(dto.getLogin(), dto.getPassword(), Utilisateur.ROLES.JOURNALISTE);
        em.persist(u);
        return u;
    }

    @Transactional
    public List<Journal> getJournaux(Journal.STATUS status, boolean fetchArticles){
        String query = fetchArticles ? "select distinct j from Journal j left join fetch j.articles a where j.status = :status"
                                     : "select j from Journal j where j.status = :status";

        List<Journal> list = em.createQuery(query, Journal.class)
                .setParameter("status", status)
                .getResultList();

        return list;
    }

    @Transactional
    public Journal getJournal(int idJ){
        try{
            List<Journal> j = em.createQuery("select distinct j from Journal j left join fetch j.articles a where j.idJ = :idJ", Journal.class)
                    .setParameter("idJ", idJ)
                    .getResultList();

            return j.get(0);
        }
        catch (Exception e){
            return null;
        }
    }

    @Transactional
    public List<Article> getArticles(Article.STATUS status){
        List<Article> list = em.createQuery("select a from Article a where a.status = :status", Article.class)
                .setParameter("status", status)
                .getResultList();
        return list;
    }

    @Transactional
    public Article getArticle(int idA) {
        try {
            Article a = em.createQuery("select a from Article a where a.idA = :idA", Article.class)
                    .setParameter("idA", idA)
                    .getSingleResult();

            return a;
        } catch (Exception e) {
            return null;
        }
    }

    @Transactional
    public boolean rejeterArticle(int idA){
        try{
            Article a = em.createQuery("select a from Article a where a.idA = :idA and a.status = :status", Article.class)
                    .setParameter("idA", idA)
                    .setParameter("status", Article.STATUS.ATTENTE)
                    .getSingleResult();

            a.setStatus(Article.STATUS.REJETE);
            return true;
        }
        catch (Exception e){
            return false;

        }
    }

    @Transactional
    public boolean ajouterArticle(Integer idA, Integer idJ){
        try{
            Article a = em.createQuery("select a from Article a where a.idA = :idA", Article.class)
                    .setParameter("idA", idA)
                    .getSingleResult();

            Journal j = em.createQuery("select j from Journal j where j.idJ = :idJ", Journal.class)
                    .setParameter("idJ", idJ)
                    .getSingleResult();

            a.setStatus(Article.STATUS.VALIDER);
            a.setJournal(j);
            a.setPosition(j.getArticles().size());
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    @Transactional
    public Article redaction(RedactionFormDto dto, Utilisateur auteur){
        String content = dto.getContenu().replaceAll("[\n]+", "\n");
        Article a = new Article(dto.getTitre(), content, Article.STATUS.ATTENTE, dto.getTheme(), auteur);
        em.persist(a);
        return a;
    }

    @Transactional
    public void creerJournal(){
        Integer i = em.createQuery("select coalesce(max(j.numero), -1) from Journal j", Integer.class)
                .getSingleResult();

        Journal j = new Journal(i+1);
        em.persist(j);
    }

    @Transactional
    public boolean publierJournal(int idJ){
        try{
            Journal j = em.createQuery("select j from Journal j where j.idJ = :idJ", Journal.class)
                    .setParameter("idJ", idJ)
                    .getSingleResult();

            j.setStatus(Journal.STATUS.PUBLIE);
            j.setDatePublication(new Date(System.currentTimeMillis()));
            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    @Transactional
    public boolean deplacerArticle(int idJ, int idA, int newPosition){
        try{
            Journal j = em.createQuery("select j from Journal j left join fetch j.articles where j.idJ = :idJ and j.status = :status", Journal.class)
                    .setParameter("idJ", idJ)
                    .setParameter("status", Journal.STATUS.ATTENTE)
                    .getSingleResult();

            Article a = em.createQuery("select a from Article a where a.idA = :idA and a.journal = :idJ", Article.class)
                    .setParameter("idA", idA)
                    .setParameter("idJ", j)
                    .getSingleResult();

            if(newPosition < 0 || newPosition >= j.getArticles().size()) return false;

            Integer lastPos = a.getPosition();
            a.setPosition(newPosition);
            j.getArticles().get(newPosition).setPosition(lastPos);

            return true;
        }
        catch (Exception e){
            return false;
        }
    }

    @Transactional
    public void rejeterArticleJournal(int idJ, int idA){
        Article a = em.createQuery("select a from Article a where a.idA = :idA and a.journal.idJ = :idJ", Article.class)
                .setParameter("idA", idA)
                .setParameter("idJ", idJ)
                .getSingleResult();

        a.setStatus(Article.STATUS.ATTENTE);
        a.setJournal(null);
    }
}
