# Le Super Journal
## Description
Mini projet scolaire en binome de création d'un mini journal en ligne.

## Technologies
- Java 11
- Spring MVC
- Thymeleaf

## Séparation des tâches
- Analyse faite à 2
- Romain :
    - Structure du projet
    - Création des entités
    - Rejet et attribution des articles
    - Ordonnancement des articles d'un journal
- Hortense :
    - Page d'accueil
    - Visualisation d'un journal
    - Écriture d'un article
    - Rédaction du scénario de test

## Scénario de test
- Ajouter la configuration Tomcat EE local server puis lancer l'application.
- Sur la page "Accueil", cliquer sur le Journal 3 pour lire le journal.
- Se connecter avec les identifiants `toto / toto`
  - Aller sur la page "rédaction d'un article" et ajouter un article
- Se déconnecter puis se connecter avec les identifiants `assist / assist`
  - Aller sur la page "Trier les articles"
  - Rejeter le premier article dans visualiser
  - Ajouter le deuxième article à "Journal 2" dans visualiser
- Se déconnecter puis se connecter avec les identifiants `chef / chef`
  - Aller sur la page "Gérer les journaux"
  - Créer un nouveau journal
  - Dans visualiser Journal 2, vous pouvez changer l'ordre des articles avec le menu déroulant à côté du titre.
  - Publier le journal.
  - Retourner dans "Gérer les journaux", puis dans visualiser le Journal 4, supprimer l'article.
  - Vérifier que le Journal 4 ne peut pas être publié sans article.
  - Retourner dans "Accueil" pour vérifier que Journal 2 apparait.
